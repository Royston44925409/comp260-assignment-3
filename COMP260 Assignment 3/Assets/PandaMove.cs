﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PandaMove : MonoBehaviour {


    public float speed;
    public float jumpForce;
    private float moveInput;

    private Rigidbody2D rb;
    private bool facingRight = true;

    private bool isGrounded;
    public Transform groundCheck;

    public float checkRadius;
    public LayerMask whatIsGround;

    bool jump = false;

    private Animator animator;

    

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();

        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if(Input.GetButtonDown("Jump1") && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;

        }
       
    }

    void FixedUpdate()
    {


        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        animator.SetBool("isGrounded", isGrounded);

        moveInput = Input.GetAxis("Horizontal1");
        Debug.Log(moveInput);
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        if(facingRight == false  && moveInput > 0)
        {
            Flip();
     
        }
        else if( facingRight == true && moveInput < 0)
        {
            Flip();
        }

        float characterSpeed = Mathf.Abs(rb.velocity.x);
        animator.SetFloat("Speed", characterSpeed);

    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
}

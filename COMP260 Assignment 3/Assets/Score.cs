﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public float timeStart;
    public Text textBox;
    public Text BtmText;


    bool timerActive = false;

    // Use this for initialization
    void Start()
    {
        textBox.text = timeStart.ToString("F2");
    }

    // Update is called once per frame
    void Update()
    {
        
            timeStart += Time.deltaTime;
            textBox.text = timeStart.ToString("F2");

     
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(other.gameObject);
        Time.timeScale = 0f;
    }
}

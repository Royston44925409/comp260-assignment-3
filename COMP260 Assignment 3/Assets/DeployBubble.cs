﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployBubble : MonoBehaviour {

    public GameObject bubblePrefab;
    public float respawnTime = 5.0f;
    private Vector2 screenBounds;
         


	// Use this for initialization
	void Start () {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        StartCoroutine(bubbleWave());
    }

    private void spawnBubble()
    {
        GameObject a = Instantiate(bubblePrefab) as GameObject;
        a.transform.position = new Vector2(screenBounds.x * -2, Random.Range(-screenBounds.y, screenBounds.y));
    }
	
	IEnumerator bubbleWave()
    {
        while (true)
        {
        yield return new WaitForSeconds(respawnTime);
        spawnBubble();
        }
            
    }
}
